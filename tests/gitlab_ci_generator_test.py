from unittest import TestCase
from unittest.mock import patch, mock_open
import os
import src.gitlab_ci_generator_package.gitlab_ci_generator


class TestGitlabCiGenerator(TestCase):

    def test_parseYaml(self):
        from src.gitlab_ci_generator_package.gitlab_ci_generator\
            import parseYaml
        testDir = os.path.realpath(
            os.path.join(
                os.path.dirname(__file__), 'files/'
                )
            )
        inputFile = os.path.join(testDir, "test.yaml")
        expected = {'favorite_fruits': ['apple', 'pear', 'orange']}
        actual = parseYaml(inputFile)
        self.assertEqual(expected, actual)

    def test_readJinjaTemplate(self):
        from src.gitlab_ci_generator_package.gitlab_ci_generator\
            import readJinjaTemplate
        testDir = os.path.realpath(
            os.path.join(
                os.path.dirname(__file__), 'files/'
                )
            )
        inputFile = os.path.join(testDir, "test.jinja")
        dictTest = {'name': 'test'}
        expected = "\ntest\n"
        actual = readJinjaTemplate(inputFile).render(test=dictTest)
        self.assertEqual(expected, actual)
