export PROJECT_NAME=$(echo $PWD | xargs basename)
docker run --pull always --name $PROJECT_NAME \
    --rm -it \
    -v $PWD:/home/developer/$PROJECT_NAME/ \
    gschaetz/python:latest \
    /bin/bash -c \
    "cd $PROJECT_NAME/
     echo '0.0.0' > VERSION.txt 
     pip install --no-cache-dir -r requirements.txt
     /bin/bash
    "